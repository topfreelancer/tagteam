//
//  AppDelegate.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit
import IQKeyboardManagerSwift
import GoogleMaps
import GooglePlaces

var thisuser:UserModel?

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        
        GMSServices.provideAPIKey("AIzaSyCf0jujqzjJwsUF8-7Bgn5yPIfkaA0LYOs")
        IQKeyboardManager.shared.enable = true
        
        thisuser = UserModel()
        thisuser?.loadUserInfo()
        thisuser?.saveUserInfo()
        GMSPlacesClient.provideAPIKey(GOOGLE_PLACES_API_KEY)
        
        return true
    }
    
        // MARK: UISceneSession Lifecycle

    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }

}


