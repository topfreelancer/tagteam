//
//  PendingRequestVC.swift
//  TagTeam
//
//  Created by top Dev on 10/5/20.
//

import UIKit

class PendingRequestVC: BaseVC {

    @IBOutlet weak var tbv_pendingRequest: UITableView!
    var ds_pendingRuquests = [OverviewModel]()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataSource()
    }
    
    func initUI() {
        self.setTitle("Pending Request")
        setNavigationBarItem(icon : Constant.ICON_MENU!)
    }
    
    func getDataSource()  {
        self.ds_pendingRuquests.removeAll()
        var num = 0
        for i in 0 ... 9{
            num += 1
            let str_date = getStringFormDate(date: Date())
            
            self.ds_pendingRuquests.append(OverviewModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i], request_type: Constant.REQUEST_TYPE[i % 3], distance: Constant.DISTANCE[i], industry: Constant.INDUSTRY[i % 9], date: str_date, content_type: TestData.content[i], content_sourcing: TestData.content[i], status: TestData.status[i % 4], price: Float(TestData.price[i])))
            
            //self.ds_pendingRuquests.append(FollowerModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i]))
            if num == 10{
                self.tbv_pendingRequest.reloadData()
            }
        }
        /*self.showLoadingView(vc: self)
        ApiManager.getNotys { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_pendingRuquests.removeAll()
                let dict = JSON(data as Any)
                let notification_info = dict["notification_info"].arrayObject
                var num = 0
                if let notificationinfo = notification_info{
                    if notificationinfo.count != 0{
                        for one in notificationinfo{
                            num += 1
                            let jsonone = JSON(one as Any)
                            
                            let type = jsonone["type"].stringValue
                                if type == "follow" || type == "like"{
                                    let notimodel = NotiModel(id: jsonone["id"].stringValue,owner_id: jsonone["owner_id"].stringValue, user_id: jsonone["user_id"].stringValue, user_name: jsonone["user_name"].stringValue, photo_url: jsonone["user_photo"].stringValue, type: jsonone["type"].stringValue)
                                    self.ds_pendingRuquests.append(notimodel)
                                }
                            
                            
                            
                            if num == notificationinfo.count{
                                self.tbv_pendingRequest.reloadData()
                            }
                        }
                    }else{
                        //まだ通知はありません。
                        print("no notifications yet.")
                        self.showToast("まだ通知はありません。")
                    }
                }
            }
        }*/
        
        
    }
}

extension PendingRequestVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_pendingRuquests.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbv_pendingRequest?.dequeueReusableCell(withIdentifier: "PendingCell", for:indexPath) as! PendingCell
        cell.selectionStyle = .none
        cell.entity = ds_pendingRuquests[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let requestDetail: RequestDetailVC = self.createVC(VCs.REQUESTDETAIL) as! RequestDetailVC
        requestDetail.selectedUser = self.ds_pendingRuquests[indexPath.section]
        self.gotoNavPresentWithVC(requestDetail, fullscreen: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}
