//
//  OverViewVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit
import DLRadioButton

class OverViewVC: BaseVC {

    @IBOutlet weak var tbv_overviews: UITableView!
    @IBOutlet weak var cus_radio_all: DLRadioButton!
    @IBOutlet weak var cus_radio_ongoing: DLRadioButton!
    @IBOutlet weak var cus_radio_cancelled: DLRadioButton!
    @IBOutlet weak var cus_radio_completed: DLRadioButton!
    @IBOutlet weak var cus_radio_underreview: DLRadioButton!
    
    var ds_overviews = [OverviewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataSource()
    }
    
    func initUI() {
        self.setTitle("Overview")
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        self.cus_radio_all.isSelected = true
    }
    
    func getDataSource()  {
        self.ds_overviews.removeAll()
        var num = 0
        for i in 0 ... 9{
            num += 1
            let str_date = getStringFormDate(date: Date())
            
            self.ds_overviews.append(OverviewModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i], request_type: Constant.REQUEST_TYPE[i % 3], distance: Constant.DISTANCE[i], industry: Constant.INDUSTRY[i % 9], date: str_date, content_type: TestData.content[i], content_sourcing: TestData.content[i], status: TestData.status[i % 4], price: Float(TestData.price[i])))
            
            //self.ds_overviews.append(FollowerModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i]))
            if num == 10{
                self.tbv_overviews.reloadData()
            }
        }
        /*self.showLoadingView(vc: self)
        ApiManager.getNotys { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_overviews.removeAll()
                let dict = JSON(data as Any)
                let notification_info = dict["notification_info"].arrayObject
                var num = 0
                if let notificationinfo = notification_info{
                    if notificationinfo.count != 0{
                        for one in notificationinfo{
                            num += 1
                            let jsonone = JSON(one as Any)
                            
                            let type = jsonone["type"].stringValue
                                if type == "follow" || type == "like"{
                                    let notimodel = NotiModel(id: jsonone["id"].stringValue,owner_id: jsonone["owner_id"].stringValue, user_id: jsonone["user_id"].stringValue, user_name: jsonone["user_name"].stringValue, photo_url: jsonone["user_photo"].stringValue, type: jsonone["type"].stringValue)
                                    self.ds_overviews.append(notimodel)
                                }
                            
                            
                            
                            if num == notificationinfo.count{
                                self.tbv_overviews.reloadData()
                            }
                        }
                    }else{
                        //まだ通知はありません。
                        print("no notifications yet.")
                        self.showToast("まだ通知はありません。")
                    }
                }
            }
        }*/
        
        
    }
}

extension OverViewVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_overviews.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbv_overviews?.dequeueReusableCell(withIdentifier: "OverviewCell", for:indexPath) as! OverviewCell
        cell.selectionStyle = .none
        cell.entity = ds_overviews[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let requestDetail: RequestDetailVC = self.createVC(VCs.REQUESTDETAIL) as! RequestDetailVC
        requestDetail.selectedUser = self.ds_overviews[indexPath.section]
        self.gotoNavPresentWithVC(requestDetail, fullscreen: true)
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

