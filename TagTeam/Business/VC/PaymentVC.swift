//
//  PaymentVC.swift
//  TagTeam
//
//  Created by top Dev on 10/5/20.
//

import UIKit

class PaymentVC: BaseVC {

    @IBOutlet weak var lbl_price: UILabel!
    var flt_price: Float!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    func initUI() {
        self.setTitle("Payment option")
        self.addleftButton()
        
        self.lbl_price.text = "$" + String(format: "%.2f", flt_price)
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(leftBtnClicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func leftBtnClicked() {
        //self.navigationController?.popViewController(animated: true)
        self.gotoNavPresent(VCs.OVERVIEW, fullscreen: true)
    }
    @IBAction func payBtnClicked(_ sender: Any) {
        self.gotoNavPresent(VCs.REVIEW, fullscreen: true)
    }
}
