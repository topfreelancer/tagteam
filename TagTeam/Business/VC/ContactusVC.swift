//
//  ContactusVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class ContactusVC: BaseVC {

    @IBOutlet weak var txv_content: UITextView!
    @IBOutlet weak var edt_title: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    func initUI() {
        self.setTitle("Contact us")
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        txv_content.text = "Input content here"
        txv_content.textColor = UIColor.lightGray
        self.txv_content.delegate = self
        setEdtPlaceholder(self.edt_title, placeholderText: "", placeColor: .lightGray, padding: .left(10))
    }
    
    @IBAction func submitBtnClicked(_ sender: Any) {
        
    }
}

extension ContactusVC: UITextViewDelegate{
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.black
        }
    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "Input content here"
            textView.textColor = UIColor.lightGray
        }
    }
}
