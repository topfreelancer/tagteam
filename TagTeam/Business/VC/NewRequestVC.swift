//
//  NewRequestVC.swift
//  TagTeam
//
//  Created by Admin on 10/2/20.
//

import UIKit
import ActionSheetPicker_3_0

class SendNewRequestVC: BaseVC {
    
    @IBOutlet weak var lbl_requesttype: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_industry: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var txv_detail: UITextView!
    @IBOutlet weak var edt_contenttype: UITextField!
    @IBOutlet weak var edt_contentsourcing: UITextField!
    var request_type = ""
    var distance = ""
    var industry = ""
    var date = ""
    var content_type = ""
    var content_sourcing = ""
    var details = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoBack), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUI(){
        addleftButton()
        self.title = ""
        setEdtPlaceholder(self.edt_contenttype, placeholderText: "", placeColor: .lightGray, padding: .left(10))
        setEdtPlaceholder(self.edt_contentsourcing, placeholderText: "", placeColor: .lightGray, padding: .left(10))
    }
    @IBAction func tappedRequestType(_ sender: Any) {
        openBottomSheet(title: Title.request_type.rawValue, content: Constant.REQUEST_TYPE, sender: sender)
    }
    
    @IBAction func tappedDistance(_ sender: Any) {
        openBottomSheet(title: Title.distance_from_city.rawValue, content: Constant.DISTANCE, sender: sender)
    }
    
    @IBAction func tappedIndustry(_ sender: Any) {
        openBottomSheet(title: Title.select_industry.rawValue, content: Constant.INDUSTRY, sender: sender)
    }
    
    func openBottomSheet(title : String, content : [String], sender : Any) {
        ActionSheetStringPicker.show(withTitle: title, rows: content, initialSelection: 1, doneBlock: {
            picker, value, index in
            
            switch title{
            case Title.request_type.rawValue:
                self.lbl_requesttype.text = "   " + "\(index ?? "")"
                break
            case Title.distance_from_city.rawValue:
                self.lbl_distance.text = "   " + "\(index ?? "")"
                break
            case Title.select_industry.rawValue:
                self.lbl_industry.text = "   " + "\(index ?? "")"
                break
                
            default:
                print("default")
            }
            /*print("picker = \(String(describing: picker))")
            print("value = \(value)")
            print("index = \(String(describing: index))")*/
            return
            
        }, cancel: { picker in
            return
            
        }, origin: sender)
    }
    
    @IBAction func tappedCalendar(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "Date", datePickerMode: UIDatePicker.Mode.date, selectedDate: NSDate() as Date?, doneBlock: {
            picker, value, index in
            self.lbl_date.text = "   " + "\(value ?? "")"
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        
        datePicker?.show()
    }
    @IBAction func confirmBtnClicked(_ sender: Any) {
        // getting parameters
        self.request_type = self.lbl_requesttype.text ?? ""
        self.distance = self.lbl_distance.text ?? ""
        self.industry = self.lbl_industry.text ?? ""
        self.date = self.lbl_date.text ?? ""
        self.content_type = self.edt_contenttype.text ?? ""
        self.content_sourcing = self.edt_contentsourcing.text ?? ""
        self.details = self.txv_detail.text
        if request_type.isEmpty{
            self.showToast(Messages.REQUEST_TYPE_REQUIRE)
            return
        }
        if  distance.isEmpty{
            self.showToast(Messages.DISTANCE_REQUIRE)
            return
        }
        if  industry.isEmpty{
            self.showToast(Messages.INDUSTRY_REQUIRE)
            return
        }
        if  date.isEmpty{
            self.showToast(Messages.DATE_REQUIRE)
            return
        }
        if  content_type.isEmpty{
            self.showToast(Messages.CONTENT_TYPE_REQUIRE)
            return
        }
        if  content_sourcing.isEmpty{
            self.showToast(Messages.CONTENT_SOURCING_REQUIRE)
            return
        }
        if  details.isEmpty{
            self.showToast(Messages.DETAILS_REQUIRE)
            return
        }else{
            // upload reques to the server
            self.showLoadingView(vc: self)
            ApiManager.uploadNewRequest(request_type: self.request_type, distance: self.distance, industry: self.industry, date: self.date, content_type: self.content_type, content_sourcing: self.content_sourcing, details: self.details) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let toVC = self.createVC(VCs.INFLUENCEDETAIL) as! InfluenceDetailVC
                    toVC.industry_name = self.industry
                    self.gotoNavPresentWithVC(toVC, fullscreen: true)
                    
                }else{
                    self.showToast(Messages.NETISSUE)
                }
            }
        }
        
    }
}

enum Title: String {
    case request_type = "Select request type"
    case select_industry = "Select Industry"
    case distance_from_city = "Distance from city"
}

