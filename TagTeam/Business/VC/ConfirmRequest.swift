//
//  ConfirmRequest.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class ConfirmRequest: BaseVC {

    @IBOutlet weak var imv_avatar: UIImageView!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()

    }
    
    func initUI() {
        self.setTitle("Confirm Request")
        self.addleftButton()
        let url = URL(string: thisuser?.user_photo ?? "")
        imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(leftBtnClicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func leftBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }
}
