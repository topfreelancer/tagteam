//
//  B_MenuVC.swift
//  TagTeam
//
//  Created by Admin on 10/2/20.
//

import UIKit

class B_MenuVC: BaseVC {
    
    let MENU_ITEMS4BUSINESS : [String] = ["Inbox", "New Request", "Overview", "Calendar", "Profile", "Settings", "Contact us", "Logout"]
    let MENU_ITEMS4INFLUENCE : [String] = ["Inbox", "Pending Request", "Completed Request", "Calendar", "Profile", "Settings", "Contact us", "Logout"]
    
    @IBOutlet weak var tbvMenu: UITableView!
    
    var inboxVC : UIViewController!
    var newRequestNav : UINavigationController!
    var settingNav : UINavigationController!
    var profileNav : UINavigationController!
    var contactNav : UINavigationController!
    var overViewNav : UINavigationController!
    var calendarNav : UINavigationController!
    var pendingRequestNav : UINavigationController!
    var completedRequestNav : UINavigationController!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        //  init ViewControllers
        let newRequestVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.NEWREQUEST)
        self.newRequestNav = UINavigationController(rootViewController: newRequestVC)
        
        let settingsVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.SETTINGS)
        self.settingNav = UINavigationController(rootViewController: settingsVC)
        
        let profileVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.PROFILE)
        self.profileNav = UINavigationController(rootViewController: profileVC)
        
        let contactVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.CONTACTUS)
        self.contactNav = UINavigationController(rootViewController: contactVC)
        
        let overViewVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.OVERVIEW)
        self.overViewNav = UINavigationController(rootViewController: overViewVC)
        
        let calendarVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.CALENDAR)
        self.calendarNav = UINavigationController(rootViewController: calendarVC)
        
        let pendingRequestVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.PENDINGREQUEST)
        self.pendingRequestNav = UINavigationController(rootViewController: pendingRequestVC)
        
        let completedRequestVC = MainStoryboard.instantiateViewController(withIdentifier: VCs.COMPLETEDREQUEST)
        self.completedRequestNav = UINavigationController(rootViewController: completedRequestVC)
        
        tbvMenu.delegate = self
        tbvMenu.dataSource = self
        tbvMenu.tableFooterView = UIView(frame: .zero)
        tbvMenu.rowHeight = 60
    }
}

extension B_MenuVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if thisuser!.user_type == Usertype.business.rawValue{
            return MENU_ITEMS4BUSINESS.count
        }else{
            return MENU_ITEMS4INFLUENCE.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MenuCell
        if thisuser!.user_type == Usertype.business.rawValue{
            cell.lblMenu.text = MENU_ITEMS4BUSINESS[indexPath.row]
        }else{
            cell.lblMenu.text = MENU_ITEMS4INFLUENCE[indexPath.row]
        }
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0: // inbox
            slideMenuController()?.changeMainViewController(inboxVC, close: true)
            break
        case 1: // newrequest, pendingrequest
            if thisuser!.user_type == Usertype.business.rawValue{
                slideMenuController()?.changeMainViewController(newRequestNav, close: true)
            }else{
                slideMenuController()?.changeMainViewController(pendingRequestNav, close: true)
            }
            break
        case 2: //overview, completed request
            if thisuser!.user_type == Usertype.business.rawValue{
                slideMenuController()?.changeMainViewController(overViewNav, close: true)
            }else{
                slideMenuController()?.changeMainViewController(completedRequestNav, close: true)
            }
            break
        case 3: //calendar
            slideMenuController()?.changeMainViewController(calendarNav, close: true)
            break
        case 4: //profile
            slideMenuController()?.changeMainViewController(profileNav, close: true)
            break
        case 5: //settings
            slideMenuController()?.changeMainViewController(settingNav, close: true)
            break
        case 6: //contact us
            slideMenuController()?.changeMainViewController(contactNav, close: true)
            break
        case 7: //logout
            thisuser?.clearUserInfo()
            UserDefault.setString(key: PARAMS.PASSWORD, value: nil)
            self.gotoVC(VCs.LOGIN_NAV)
            break
        default:
            break
        }
    }
}

class MenuCell: UITableViewCell {
    @IBOutlet weak var lblMenu: UILabel!
}
