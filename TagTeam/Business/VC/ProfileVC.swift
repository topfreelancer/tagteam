//
//  ProfileVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class ProfileVC: BaseVC {

    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    func initUI() {
        self.setTitle("Profile")
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        let url = URL(string: thisuser?.user_photo ?? "")
        imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
        self.lbl_username.text = thisuser?.user_name
    }
}
