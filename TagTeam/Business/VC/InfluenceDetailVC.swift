//
//  InfluenceDetailVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class InfluenceDetailVC: BaseVC {

    @IBOutlet weak var tbv_followers: UITableView!
    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_industry_name: UILabel!
    
    var ds_followers = [FollowerModel]()
    var industry_name = ""
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataSource()
    }
    
    func initUI() {
        self.setTitle("Influencer Detail")
        self.addleftButton()
        let url = URL(string: thisuser?.user_photo ?? "")
        imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
        self.lbl_industry_name.text = "Industry name:" + self.industry_name
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(leftBtnClicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func leftBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func getDataSource()  {
        self.ds_followers.removeAll()
        var num = 0
        for i in 0 ... 9{
            num += 1
            self.ds_followers.append(FollowerModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i]))
            if num == 10{
                self.tbv_followers.reloadData()
            }
        }
        /*self.showLoadingView(vc: self)
        ApiManager.getNotys { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_followers.removeAll()
                let dict = JSON(data as Any)
                let notification_info = dict["notification_info"].arrayObject
                var num = 0
                if let notificationinfo = notification_info{
                    if notificationinfo.count != 0{
                        for one in notificationinfo{
                            num += 1
                            let jsonone = JSON(one as Any)
                            
                            let type = jsonone["type"].stringValue
                                if type == "follow" || type == "like"{
                                    let notimodel = NotiModel(id: jsonone["id"].stringValue,owner_id: jsonone["owner_id"].stringValue, user_id: jsonone["user_id"].stringValue, user_name: jsonone["user_name"].stringValue, photo_url: jsonone["user_photo"].stringValue, type: jsonone["type"].stringValue)
                                    self.ds_followers.append(notimodel)
                                }
                            
                            
                            
                            if num == notificationinfo.count{
                                self.tbv_followers.reloadData()
                            }
                        }
                    }else{
                        //まだ通知はありません。
                        print("no notifications yet.")
                        self.showToast("まだ通知はありません。")
                    }
                }
            }
        }*/
        
        
    }
    @IBAction func gotoConfirmRequest(_ sender: Any) {
        self.gotoNavPresent(VCs.CONFIRMREQUEST, fullscreen: true)
    }
}

extension InfluenceDetailVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_followers.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = tbv_followers?.dequeueReusableCell(withIdentifier: "FollowerCell", for:indexPath) as! FollowerCell
        cell.selectionStyle = .none
        cell.entity = ds_followers[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}
