//
//  NewRequestVC.swift
//  TagTeam
//
//  Created by Admin on 10/2/20.
//

import UIKit
import ActionSheetPicker_3_0
import GoogleMaps

class SendNewRequestVC: BaseVC {
    
    @IBOutlet weak var lbl_requesttype: UILabel!
    @IBOutlet weak var lbl_date: UILabel!
    @IBOutlet weak var txv_detail: UITextView!
    @IBOutlet weak var edt_price: UITextField!
    
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    // from other controller
    var service_user: UserModel?
    
    var service_type = ""
    var service_date = ""
    var service_details = ""
    var service_price = ""
    var timestamp: Int = 0
    var nowtimestamp: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.isMyLocationEnabled = true
        mapView.animate(toViewingAngle: 45.0)
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
        self.setUI()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.showNavBar()
    }
    
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(gotoBack), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @objc func gotoBack() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func setUI(){
        addleftButton()
        setEdtPlaceholder(self.edt_price, placeholderText: "", placeColor: .darkGray, padding: .left(10))
        self.title = Messages.SEND_NEW_REQUEST
        self.txv_detail.textContainerInset =
            UIEdgeInsets(top: 8,left: 8,bottom: 8 ,right: 8);
        
    }
    @IBAction func tappedRequestType(_ sender: Any) {
        openBottomSheet(title: "リクエストタイプを選択", content: Constants.REQUEST_TYPE, sender: sender)
    }
    
    func openBottomSheet(title : String, content : [String], sender : Any) {
        ActionSheetStringPicker.show(withTitle: title, rows: content, initialSelection: 1, doneBlock: {
            picker, value, index in
            self.lbl_requesttype.text = "   " + "\(index ?? "")"
        
            /*print("picker = \(String(describing: picker))")
            print("value = \(value)")
            print("index = \(String(describing: index))")*/
            return
            
        }, cancel: { picker in
            return
            
        }, origin: sender)
    }
    
    @IBAction func tappedCalendar(_ sender: UIButton) {
        let datePicker = ActionSheetDatePicker(title: "日付", datePickerMode: UIDatePicker.Mode.date, selectedDate: NSDate() as Date?, doneBlock: {
            picker, value, index in
            
            if let datee = value as? Date{
                self.nowtimestamp = Int(NSDate().timeIntervalSince1970) * 1000
                self.timestamp = Int(datee.timeIntervalSince1970) * 1000
            }
            self.lbl_date.text = "   " + "\(value ?? "")"
            return
        }, cancel: { ActionStringCancelBlock in return }, origin: sender.superview!.superview)
        
        datePicker?.show()
    }

    @IBAction func confirmBtnClicked(_ sender: Any) {
        // getting parameters
        self.service_type = self.lbl_requesttype.text ?? ""
        self.service_date = self.lbl_date.text ?? ""
        self.service_details = self.txv_detail.text
        self.service_price = self.edt_price.text ?? ""
        if service_type.isEmpty{
            self.showAlerMessage(message: Messages.REQUEST_TYPE_REQUIRE)
            return
        }
        
        if  service_date.isEmpty{
            self.showAlerMessage(message: Messages.DATE_REQUIRE)
            return
        }
        
        if self.timestamp < self.nowtimestamp{
            self.showAlerMessage(message: Messages.SELECT_VALID_DATE)
            return
        }
        
        if  service_price.isEmpty{
            self.showAlerMessage(message: Messages.PRICE_REQUIRE)
            return
        }
        
        if  service_details.isEmpty{
            self.showAlerMessage(message: Messages.DETAILS_REQUIRE)
            return
        }else{
            if let user = self.service_user{
                self.showLoadingView(vc: self)
                print(timestamp)
                ApiManager.manageServiceRequest(service_id: nil, user_id:user.user_id , service_type: self.service_type, request_type: .send, service_time: "\(timestamp)", service_price: self.service_price.toInt() ?? 0, service_description: self.service_details) { (isSuccess, data) in
                    if isSuccess{
                        self.gotoTabControllerWithIndex(0)
                    }else{
                        self.showAlerMessage(message: Messages.NETISSUE)
                    }
                }
            }
        }
    }
}

extension SendNewRequestVC : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let location = locations.last
        let camera = GMSCameraPosition.camera(withLatitude: (location?.coordinate.latitude)!, longitude: (location?.coordinate.longitude)!, zoom: 17.0)
        self.mapView?.animate(to: camera)
        //Finally stop updating location otherwise it will come again and again in this delegate
        self.locationManager.stopUpdatingLocation()
        mapView.settings.myLocationButton = true
        
        /*let newLocation = locations.last // find your device location
         mapView.camera = GMSCameraPosition.camera(withTarget: newLocation!.coordinate, zoom: 14.0) // show your device location on map
         mapView.settings.myLocationButton = true // show current location button
         var lat = (newLocation?.coordinate.latitude)! // get current location latitude
         var long = (newLocation?.coordinate.longitude)! //get current location longitude*/
    }
}

