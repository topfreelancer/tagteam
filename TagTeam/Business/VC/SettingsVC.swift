//
//  SettingsVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class SettingsVC: BaseVC {

    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_phonenumber: UITextField!
    @IBOutlet weak var edt_location: UITextField!
    @IBOutlet weak var edt_industry: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUI()
    }
    
    func setUI() {
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        self.title = "Settings"
        self.setEdtPlaceholder(self.edt_email, placeholderText: "", placeColor: .lightGray, padding: .left(10))
        self.setEdtPlaceholder(self.edt_phonenumber, placeholderText: "", placeColor: .lightGray, padding: .left(10))
        self.setEdtPlaceholder(self.edt_location, placeholderText: "", placeColor: .lightGray, padding: .left(10))
        self.setEdtPlaceholder(self.edt_industry, placeholderText: "", placeColor: .lightGray, padding: .left(10))
    }
}
