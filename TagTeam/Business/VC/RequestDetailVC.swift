//
//  RequestDetailVC.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import UIKit

class RequestDetailVC: BaseVC {

    @IBOutlet weak var imv_avatar: UIImageView!
    @IBOutlet weak var lbl_username: UILabel!
    @IBOutlet weak var lbl_request_type: UILabel!
    @IBOutlet weak var lbl_distance: UILabel!
    @IBOutlet weak var lbl_industry: UILabel!
    @IBOutlet weak var lbl_content_type: UILabel!
    @IBOutlet weak var lbl_content_sourcing: UILabel!
    @IBOutlet weak var lbl_ongoing: UILabel!
    @IBOutlet weak var lbl_completed: UILabel!
    @IBOutlet weak var lbl_canceled: UILabel!
    @IBOutlet weak var lbl_underreview: UILabel!
    @IBOutlet weak var lbl_price: UILabel!
    var selectedUser: OverviewModel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
    }
    
    func initUI() {
        self.setTitle("Request Detail")
        self.addleftButton()
        
        let url = URL(string: selectedUser.userPhoto ?? "")
        imv_avatar.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
        self.lbl_username.text = selectedUser.userName
        self.lbl_request_type.text = "Request type:" + " " + selectedUser.request_type!
        self.lbl_distance.text = "Distance:" + " " + selectedUser.distance!
        self.lbl_industry.text = "Industry:" + " " + selectedUser.industry!
        self.lbl_content_type.text = "Content type:" + " " + selectedUser.content_type!
        self.lbl_content_sourcing.text = "Content sourcing:" + " " + selectedUser.content_sourcing!
        self.lbl_price.text = "$" + String(format: "%.2f", selectedUser.price)
        setStatus(selectedUser.status!)
    }
    
    func setStatus(_ status: status)  {
        switch status {
        case .ongoing:
            lbl_ongoing.textColor = .blue
            lbl_completed.textColor = .lightGray
            lbl_canceled.textColor = .lightGray
            lbl_underreview.textColor = .lightGray
        case .completed:
            lbl_ongoing.textColor = .lightGray
            lbl_completed.textColor = .blue
            lbl_canceled.textColor = .lightGray
            lbl_underreview.textColor = .lightGray
        case .canceled:
            lbl_ongoing.textColor = .lightGray
            lbl_completed.textColor = .lightGray
            lbl_canceled.textColor = .blue
            lbl_underreview.textColor = .lightGray
        case .under_review:
            lbl_ongoing.textColor = .lightGray
            lbl_completed.textColor = .lightGray
            lbl_canceled.textColor = .lightGray
            lbl_underreview.textColor = .blue
        }
    }
    
    func addleftButton() {
        let btn_back = UIButton(type: .custom)
        btn_back.setImage(UIImage (named: "ic_back")!.withRenderingMode(.alwaysTemplate), for: .normal)
        btn_back.addTarget(self, action: #selector(leftBtnClicked), for: .touchUpInside)
        btn_back.imageEdgeInsets = UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5)
        btn_back.tintColor = UIColor.white
        let barButtonItemBack = UIBarButtonItem(customView: btn_back)
        barButtonItemBack.customView?.widthAnchor.constraint(equalToConstant: 35).isActive = true
        barButtonItemBack.customView?.heightAnchor.constraint(equalToConstant: 35).isActive = true
        self.navigationItem.leftBarButtonItem = barButtonItemBack
    }
    
    @IBAction func payBtnClickeed(_ sender: Any) {
        let paymentVC = self.createVC(VCs.PAYMENT) as! PaymentVC
        paymentVC.flt_price = self.selectedUser.price
        self.gotoNavPresentWithVC(paymentVC, fullscreen: true)
    }
    
    @objc func leftBtnClicked() {
        self.navigationController?.popViewController(animated: true)
    }
}
