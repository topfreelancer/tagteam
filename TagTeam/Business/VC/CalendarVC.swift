//
//  CalendarVC.swift
//  TagTeam
//
//  Created by top Dev on 10/5/20.
//

import UIKit

class CalendarVC: BaseVC {

    @IBOutlet weak var tbv_overviews: UITableView!
    @IBOutlet weak var calendarViewV: CalendarView!
    @IBOutlet weak var cons_h_tbv: NSLayoutConstraint!
    var heredate: Date? = nil
    var ds_overviews = [OverviewModel]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.initUI()
        setCalendarView()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getDataSource()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let today = Date()
        var tomorrowComponents = DateComponents()
        tomorrowComponents.day = 0 // selected date setting
        #if KDCALENDAR_EVENT_MANAGER_ENABLED
        self.calendarViewV.loadEvents() { error in
            if error != nil {
                let message = "The karmadust calender could not load system events. It is possibly a problem with permissions"
                let alert = UIAlertController(title: "Events Loading Error", message: message, preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                self.present(alert, animated: true, completion: nil)
            }
        }
        #endif
        
        if today.dayofTheWeek != ""{
            print("This is the weekday number===>,\(today.dayofTheWeek)")
        }
        
        self.calendarViewV.setDisplayDate(today)
    }
    
    func initUI() {
        self.setTitle("Calendar")
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        self.cons_h_tbv.constant = Constants.SCREEN_HEIGHT - topbarHeight - 65
        if #available(iOS 11.0, *) {
            if let window = UIApplication.shared.keyWindow {
                let safeAreaBottom = window.safeAreaInsets.bottom
                let safeAreaTop = window.safeAreaInsets.top
                self.cons_h_tbv.constant = Constants.SCREEN_HEIGHT - topbarHeight - 65 - safeAreaBottom - safeAreaTop
            }
        }
    }
    
    func setCalendarView()  {
        let style = CalendarView.Style()
        style.cellShape                = .round
        style.cellColorDefault         = UIColor.clear
        style.cellColorToday           = UIColor.systemRed
        style.cellSelectedBorderColor  = UIColor.systemRed
        style.cellEventColor           = UIColor.clear
        style.headerTextColor          = UIColor.darkGray
        style.cellTextColorDefault     = UIColor.black
        style.cellTextColorToday       = UIColor.white
        style.cellTextColorWeekend     = UIColor.black
        //style.cellColorOutOfRange      = UIColor(red: 249/255, green: 226/255, blue: 212/255, alpha: 1.0)
        style.cellColorOutOfRange      = .lightGray
        style.headerBackgroundColor    = UIColor.white
        style.weekdaysBackgroundColor  = UIColor.white
        style.firstWeekday             = .sunday
        style.locale                   = Locale(identifier: Constants.localTimeZoneIdentifier)
        style.cellFont = UIFont(name: "Helvetica", size: 20.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.headerFont = UIFont(name: "Helvetica", size: 24.0) ?? UIFont.systemFont(ofSize: 20.0)
        style.weekdaysFont = UIFont(name: "Helvetica", size: 14.0) ?? UIFont.systemFont(ofSize: 14.0)
        calendarViewV.style = style
        calendarViewV.dataSource = self
        calendarViewV.delegate = self
        calendarViewV.direction = .horizontal
        calendarViewV.multipleSelectionEnable = false
        calendarViewV.marksWeekends = true
        calendarViewV.backgroundColor = UIColor(red: 252/255, green: 252/255, blue: 252/255, alpha: 1.0)
    }
    
    func getDataSource()  {
        self.ds_overviews.removeAll()
        var num = 0
        for i in 0 ... 9{
            num += 1
            let str_date = getStringFormDate(date: Date())
            
            self.ds_overviews.append(OverviewModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i], request_type: Constant.REQUEST_TYPE[i % 3], distance: Constant.DISTANCE[i], industry: Constant.INDUSTRY[i % 9], date: str_date, content_type: TestData.content[i], content_sourcing: TestData.content[i], status: TestData.status[i % 4], price: Float(TestData.price[i])))
            
            //self.ds_overviews.append(FollowerModel(id: "\(i)", userName: TestData.userNames[i], userPhoto: TestData.images[i]))
            if num == 10{
                self.tbv_overviews.reloadData()
            }
        }
        /*self.showLoadingView(vc: self)
        ApiManager.getNotys { (isSuccess, data) in
            self.hideLoadingView()
            if isSuccess{
                self.ds_overviews.removeAll()
                let dict = JSON(data as Any)
                let notification_info = dict["notification_info"].arrayObject
                var num = 0
                if let notificationinfo = notification_info{
                    if notificationinfo.count != 0{
                        for one in notificationinfo{
                            num += 1
                            let jsonone = JSON(one as Any)
                            
                            let type = jsonone["type"].stringValue
                                if type == "follow" || type == "like"{
                                    let notimodel = NotiModel(id: jsonone["id"].stringValue,owner_id: jsonone["owner_id"].stringValue, user_id: jsonone["user_id"].stringValue, user_name: jsonone["user_name"].stringValue, photo_url: jsonone["user_photo"].stringValue, type: jsonone["type"].stringValue)
                                    self.ds_overviews.append(notimodel)
                                }
                            
                            
                            
                            if num == notificationinfo.count{
                                self.tbv_overviews.reloadData()
                            }
                        }
                    }else{
                        //まだ通知はありません。
                        print("no notifications yet.")
                        self.showToast("まだ通知はありません。")
                    }
                }
            }
        }*/
        
        
    }
}

extension CalendarVC: UITableViewDataSource, UITableViewDelegate{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.ds_overviews.count
    }
    
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tbv_overviews?.dequeueReusableCell(withIdentifier: "OverviewCell", for:indexPath) as! OverviewCell
        cell.selectionStyle = .none
        cell.entity = ds_overviews[indexPath.section]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        /*let requestDetail: RequestDetailVC = self.createVC(VCs.REQUESTDETAIL) as! RequestDetailVC
        requestDetail.selectedUser = self.ds_overviews[indexPath.section]
        self.gotoNavPresentWithVC(requestDetail, fullscreen: true)*/
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 0
    }
}

extension CalendarVC: CalendarViewDataSource {
    // MARK:here we can add start limited day
    func startDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.year = -100
        let seconds = TimeZone.current.secondsFromGMT()
        print("seconds =====> ",seconds)
        let hour = seconds / 3600
        dateComponents.hour = hour
        let today = Date()
        let threeMonthsAgo = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)! // here can set the start date
        return threeMonthsAgo
    }
    // MARK: here we can add start limited day
    func endDate() -> Date {
        var dateComponents = DateComponents()
        dateComponents.month = 1200
        let today = Date()
        let twoYearsFromNow = self.calendarViewV.calendar.date(byAdding: dateComponents, to: today)!
        return twoYearsFromNow
    }
}

extension CalendarVC: CalendarViewDelegate {
    
    func calendar(_ calendar: CalendarView, didSelectDate date : Date, withEvents events: [CalendarEvent]) {
        heredate = date
        //print("Did Select: \(date) with \(events.count) events")
    }

    func calendar(_ calendar: CalendarView, didScrollToMonth date : Date) {
        print(self.calendarViewV.selectedDates)
        //self.datePicker.setDate(date, animated: true)
    }
}
