//
//  Constants.swift
//  emoglass
//
//  Created by Mac on 7/7/20.
//  Copyright © 2020 Mac. All rights reserved.
//

import Foundation
import UIKit

public let GOOGLE_PLACES_API_KEY = "AIzaSyDIy5OhjI6sI_SpqwMc-VIrdkWSRQfI6Ic"
struct Constants {
    static let PROGRESS_COLOR      = UIColor.black
    static let SAVE_ROOT_PATH      = "save_root"
    static let SCREEN_HEIGHT       = UIScreen.main.bounds.height
    static let SCREEN_WIDTH        = UIScreen.main.bounds.width
    static let deviceToken         = UIDevice.current.identifierForVendor!.uuidString
    // MARK: current timezone
    static var localTimeZoneIdentifier: String { return TimeZone.current.identifier }
}

struct VCs {
    static let SPLASH           = "SplashVC"
    static let LOGIN            = "LoginVC"
    static let LOGIN_NAV        = "LoginNav"
    static let REGISTER         = "RegisterVC"
    static let REGISTERAS       = "RegisterAsVC"
    static let INBOX            = "InboxVC"
    static let INBOXDETAIL      = "InboxDetailVC"
    static let NEWREQUEST       = "NewRequestVC"
    static let INFLUENCEDETAIL  = "InfluenceDetailVC"
    static let CONFIRMREQUEST   = "ConfirmRequest"
    static let SETTINGS         = "SettingsVC"
    static let PROFILE          = "ProfileVC"
    static let CONTACTUS        = "ContactusVC"
    static let OVERVIEW         = "OverViewVC"
    static let REQUESTDETAIL    = "RequestDetailVC"
    static let PAYMENT          = "PaymentVC"
    static let REVIEW           = "ReviewVC"
    static let CALENDAR         = "CalendarVC"
    static let PENDINGREQUEST   = "PendingRequestVC"
    static let COMPLETEDREQUEST = "CompletedReqeustVC"
}

struct TestData {
    static let images = ["https://i.pinimg.com/236x/0c/3d/bc/0c3dbc3fc00744e032fa8278e5e16741.jpg","https://i.pinimg.com/236x/4d/b0/10/4db0106cf43cea00b8c1c4c038d9f9b1.jpg","https://i.pinimg.com/236x/39/55/e9/3955e9f28508f563f1ae7e660f96b0a8.jpg","https://i.pinimg.com/236x/14/55/81/14558108de6f31aa19f5984bfbd3cbfc.jpg","https://i.pinimg.com/236x/3d/96/f1/3d96f14098bef76fdd23f22f353099cb.jpg","https://i.pinimg.com/236x/16/d1/91/16d191a2120592b0920f243e543ab5ed.jpg","https://i.pinimg.com/236x/48/ec/56/48ec56dd4aaa2508ef71ad3d8ff8a2ff.jpg","https://i.pinimg.com/236x/78/4b/30/784b30ff549ca6598e86ca55ffc5bd9f.jpg","https://i.pinimg.com/236x/61/be/9d/61be9d15e87fd945b7339ab598d655aa.jpg","https://i.pinimg.com/236x/55/64/b5/5564b54e232a080c4fd6c025c361d379.jpg"]
    static let userNames = ["Paul Newman","Hems Bett","Robert De Niro","Tom Ellis","Brad Pitt","Bloomberg kevin","Gerard Butler","Peter Kavinsky","Richard Madden","Johnny Depp"]
    //static let userAge = ["23","25","20","26","24","19","21","20","25","24"]
    static let postTime = ["2s","5s","10s","1min","2min","5min","8min","10min","12min","20min"]
    static let userLocation = ["Fukuoka","Iyo","Gunma","Sumitomo Mitsui","Wakayama","Hokkaido","Kanagawa","Okayama","Tokyo","Suruga"]
    static let content = ["content1","content2","content3","content4","content5","content6","content7","content8","content9","content10"]
    static let price = [100.00, 130.00, 450.25, 670.55, 430.00, 230.00, 340.30, 340.00, 640.00, 670.90]
    static let status: [status] = [.ongoing,.completed,.canceled,.under_review]
}

struct Messages {
    
    static let ADD_LOCATION                   = "Please input your location."
    static let ADD_PHOTO                      = "Please select your photo."
    static let AGREE_TERMS_REQUIRE            = "Are you agree terms and conditions?\n   If you agree, please check."
    static let CANCEL                         = "Cancel"
    static let CONFIRM_PASSWORD_MATCH_REQUIRE = "Please input matched password."
    static let CONFIRM_PASSWORD_REQUIRE       = "Please confirm your password."
    static let DATE_REQUIRE                   = "Please select date."
    static let DISTANCE_REQUIRE               = "Please select distance."
    static let EMAIL_EXIST                    = "Email already exist."
    static let EMAIL_REQUIRE                  = "Please input your email."
    static let IMAGE_UPLOAD_FAIL              = "Image upload failed."
    static let INDUSTRY_REQUIRE               = "Please select inustry."
    static let LOCATION_REQUIRE               = "Please input your location."
    static let NAME_REQUIRE                   = "Please input your name."
    static let NETISSUE                       = "Network issue."
    static let PASSWORD_REQUIRE               = "Please input your password."
    static let PHONE_NUMBER_REQUIRE           = "Please input your phonenumber."
    static let REQUEST_TYPE_REQUIRE           = "Please select request type."
    static let USER_NAME_REQUIRE              = "Please input your username."
    static let VALID_EMAIL_REQUIRE            = "Please input valid email."
    static let WEBSITE_LINK_REQUIRE           = "Please input your website link."
    static let CONTENT_TYPE_REQUIRE       = "Please input content type"
    static let CONTENT_SOURCING_REQUIRE       = "Please input content sourcing."
    static let DETAILS_REQUIRE       = "Please input details."
}


