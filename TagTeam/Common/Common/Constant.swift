//
//  Constant.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//
import UIKit
class Constant {
    static let IS_BUSINESS : Int = 1
    static let IS_INFLUENCER : Int = 2
    
    static let REQUEST_TYPE : [String] = ["Self-Sourced Content", "Live Storey Content", "Influencer-Sourced Content"]
    static let DISTANCE : [String] = ["< 10Km", "< 20Km", "< 30Km", "< 40Km", "< 50Km", "< 60Km", "< 70Km", "< 80Km", "< 90Km", "< 100Km"]
    static let INDUSTRY : [String] = ["Automobile", "Food", "Hospital", "Makeup", "Medical", "Retailer", "Manufacturing", "Contruction", "Real Estate"]
    
    // icons
    static let ICON_MENU = UIImage(named: "ic_menu")
    static let ICON_BACK = UIImage(named: "ic_back")
    
    
    // colors
    static let COLOR_MAIN  = UIColor(named:"colorYellow")
    static let COLOR_GOOGLE = UIColor(named: "colorGoogle")
    static let COLOR_BUTTON = UIColor(named: "colorBtn")
    static let COLOR_BLUE = UIColor(named: "colorBlue")
    static let COLOR_DARK_GREY = UIColor(named: "colorDarkGrey")
    static let COLOR_GREEN = UIColor(named: "colorGreen")
    static let COLOR_INSTAGRAM = UIColor(named: "colorInstagram")
    static let COLOR_WHITE_GREY = UIColor(named: "colorWhiteGrey")
    
    // icons
    static let AVATAR : String = "avatar"
    static let IC_MENU : String = "ic_menu"
    static let IC_BACK : String = "ic_back"
    static let FACEBOOK : String = "facebook"
    static let GOOGLE : String = "google"
    static let IC_DELETE : String = "ic_delete"
    static let IC_STAR : String = "ic_star"
    static let VISA_CARD : String = "visa_card"
    static let MASTER_CARD : String = "master_card"
    static let PAYPAL : String = "paypal"
    static let DEFAULT_IMG : String = "default_img"
    static let IC_RIGHT_ARROW : String = "ic_right_arrow"
}

var MainStoryboard = UIStoryboard(name: "Main", bundle: nil)
let appDelegate: AppDelegate? = UIApplication.shared.delegate as? AppDelegate
