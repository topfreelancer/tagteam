//
//  InboxVC.swift
//  TagTeam
//
//  Created by Admin on 10/2/20.
//

import UIKit
import DLRadioButton

class InboxVC: BaseVC {
    @IBOutlet weak var tbvInbox: UITableView!
    @IBOutlet weak var cus_radio_all: DLRadioButton!
    @IBOutlet weak var cus_radio_new: DLRadioButton!
    @IBOutlet weak var cus_radio_archive: DLRadioButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setNavigationBarItem(icon : Constant.ICON_MENU!)
        self.title = "Inbox"
        
        tbvInbox.delegate = self
        tbvInbox.dataSource = self
        tbvInbox.rowHeight = 100
        self.cus_radio_all.isSelected = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}

extension InboxVC : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 10
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! InboxCell
        cell.selectionStyle = .none
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.pushNextVC(storyboardName: "Main", vcName: "InboxDetailVC")
    }
}

class InboxCell: UITableViewCell {
    
}

extension InboxVC : SlideMenuControllerDelegate {
    
    func leftWillOpen() {
        print("SlideMenuControllerDelegate: leftWillOpen")
    }
    
    func leftDidOpen() {
        print("SlideMenuControllerDelegate: leftDidOpen")
    }
    
    func leftWillClose() {
        print("SlideMenuControllerDelegate: leftWillClose")
    }
    
    func leftDidClose() {
        print("SlideMenuControllerDelegate: leftDidClose")
    }
    
    func rightWillOpen() {
        print("SlideMenuControllerDelegate: rightWillOpen")
    }
    
    func rightDidOpen() {
        print("SlideMenuControllerDelegate: rightDidOpen")
    }
    
    func rightWillClose() {
        print("SlideMenuControllerDelegate: rightWillClose")
    }
    
    func rightDidClose() {
        print("SlideMenuControllerDelegate: rightDidClose")
    }
}
