//
//  ViewController.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit
import SwiftyJSON

var networkStatus = 0
class SplashVC: BaseVC {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        DispatchQueue.main.asyncAfter(deadline: .now() + 20.0, execute: {
                if networkStatus == 0{
                    self.showAlerMessage(message: Messages.NETISSUE)
                }
                else{
                    return
                }
            }
        )
        self.checkBackgrouond()
    }
    
    func checkBackgrouond(){
        if thisuser!.isValid && thisuser!.remember_me{
            self.showLoadingView(vc: self)
            let password = thisuser!.password ?? ""
            ApiManager.login(email: thisuser!.email ?? "", password:password ) { (isSuccess, data) in
                self.hideLoadingView()
                networkStatus = 1
                if isSuccess{
                    let data = JSON(data as Any)
                    UserDefault.setString(key: PARAMS.PASSWORD, value: password)
                    UserDefault.setBool(key: PARAMS.REMEMBER_ME, value: true)
                    dump(thisuser, name: "userinfo =========>")
                    self.createMenuVC()
                    print("data =========>",data)
                    thisuser?.clearUserInfo()
                    thisuser = UserModel(data)
                    thisuser?.password = password
                    thisuser?.remember_me = true
                    thisuser?.saveUserInfo()
                    thisuser?.loadUserInfo()
                }else{
                    self.gotoVC(VCs.LOGIN_NAV)
                    networkStatus = 1
                }
            }
        }else{
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.gotoVC(VCs.LOGIN_NAV)
                networkStatus = 1
            }
        }
    }
}

