//
//  RegisterVC.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit
import BEMCheckBox
import SKCountryPicker
import GooglePlaces
import SwiftyJSON

class RegisterVC: BaseVC {
    
    @IBOutlet weak var chbAgreeTerms: BEMCheckBox!
    @IBOutlet weak var countryCode: UILabel!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var cons_contryName: NSLayoutConstraint!
    @IBOutlet weak var edt_name: UITextField!
    @IBOutlet weak var edt_user_name: UITextField!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_phone_number: UITextField!
    @IBOutlet weak var edt_location: UITextField!
    @IBOutlet weak var edt_industry: UITextField!
    @IBOutlet weak var edt_website_url: UITextField!
    @IBOutlet weak var edt_password: UITextField!
    @IBOutlet weak var edt_confirm_pwd: UITextField!
    @IBOutlet weak var cons_t_edt_password: NSLayoutConstraint!
    @IBOutlet weak var imv_avatar: UIImageView!
    var imagePicker: ImagePicker1!
    var imageFils = [String]()
    var name = ""
    var user_name = ""
    var email = ""
    var phone_number = ""
    var location = ""
    var industry = ""
    var website_link = ""
    var password = ""
    var confirmpassword = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        self.setUI()
    }
    
    func setUI() {
        chbAgreeTerms.boxType = .square
        let user_type = UserDefault.getString(key: PARAMS.USER_TYPE, defaultValue: "business")
        if user_type == Usertype.business.rawValue {
            edt_website_url.isHidden = false
            self.cons_t_edt_password.constant = 20
        }else if user_type == Usertype.influence.rawValue {
            edt_website_url.isHidden = true
            self.cons_t_edt_password.constant = -45
        }
        imv_avatar.addTapGesture(tapNumber: 1, target: self, action: #selector(onEdtPhoto))
        self.imagePicker = ImagePicker1(presentationController: self, delegate: self)
        self.edt_location.text = "NewYork"
    }
    
    @objc func onEdtPhoto(gesture: UITapGestureRecognizer) -> Void {
        self.imagePicker.present(from: view)
    }
    
    func gotoUploadProfile(_ image: UIImage?) {
        self.imageFils.removeAll()
        if let image = image{
            imageFils.append(saveToFile(image:image,filePath:"photo",fileName:randomString(length: 2))) // Image save action
            self.imv_avatar.image = image
        }
    }
    
    @IBAction func tappedBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func countryCodeClicked(_ sender: Any) {
        let countryController = CountryPickerWithSectionViewController.presentController(on: self) { [weak self] (country: Country) in

            guard let self = self else { return }

            self.countryCode.text = country.dialingCode!
            self.countryName.text = country.countryName
            self.cons_contryName.constant = CGFloat(country.countryName.count * 8)
            
            /*print("country.countryName: ", country.countryName)
            print("country.countryCode: ", country.countryCode)
            print("country.digitCountrycode: ", country.digitCountrycode)
            print("country.dialingCode: ", country.dialingCode)*/
        }
        // can customize the countryPicker here e.g font and color
        countryController.detailColor = UIColor.red
        CountryManager.shared.addFilter(.countryCode)
        CountryManager.shared.addFilter(.countryDialCode)
        
    }
    
    @IBAction func textFieldTapped(_ sender: Any) {
      edt_location.resignFirstResponder()
      let acController = GMSAutocompleteViewController()
      acController.delegate = self
      present(acController, animated: true, completion: nil)
    }
    
    @IBAction func registerBtnClicked(_ sender: Any) {
        name = self.edt_name.text ?? ""
        user_name = self.edt_user_name.text ?? ""
        email = self.edt_email.text ?? ""
        phone_number = self.edt_phone_number.text ?? ""
        location = self.edt_location.text ?? ""
        industry = self.edt_industry.text ?? ""
        website_link = self.edt_website_url.text ?? ""
        password = self.edt_password.text ?? ""
        confirmpassword = self.edt_confirm_pwd.text ?? ""
        if self.imageFils.count == 0{
            self.showToast(Messages.ADD_PHOTO)
            return
        }
        if self.name.isEmpty{
            self.showToast(Messages.NAME_REQUIRE)
            return
        }
        if self.user_name.isEmpty{
            self.showToast(Messages.USER_NAME_REQUIRE)
            return
        }
        if self.email.isEmpty{
            self.showToast(Messages.EMAIL_REQUIRE)
            return
        }
        if !self.email.isValidEmail(){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }
        if self.phone_number.isEmpty{
            self.showToast(Messages.PHONE_NUMBER_REQUIRE)
            return
        }
        if self.location.isEmpty{
            self.showToast(Messages.LOCATION_REQUIRE)
            return
        }
        if self.industry.isEmpty{
            self.showToast(Messages.INDUSTRY_REQUIRE)
            return
        }
        if self.website_link.isEmpty{
            self.showToast(Messages.WEBSITE_LINK_REQUIRE)
            return
        }
        if self.password.isEmpty{
            self.showToast(Messages.PASSWORD_REQUIRE)
            return
        }
        if self.confirmpassword.isEmpty{
            self.showToast(Messages.CONFIRM_PASSWORD_REQUIRE)
            return
        }
        if self.password != self.confirmpassword{
            self.showToast(Messages.CONFIRM_PASSWORD_MATCH_REQUIRE)
            return
        }
        if self.chbAgreeTerms.on == false{
            self.showToast(Messages.AGREE_TERMS_REQUIRE)
            return
        }
        else{
            self.showLoadingView(vc: self)
            let user_type = UserDefault.getString(key: PARAMS.USER_TYPE, defaultValue: "business")
            let remember_me = UserDefault.getBool(key: PARAMS.REMEMBER_ME,defaultValue: false)
            ApiManager.signup(user_photo: imageFils.first ?? "", name: name, user_name: user_name, email: email, phone_number: phone_number, location: location, industry: industry, website_link: website_link, password: password, user_type:user_type!){ (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    let dict = JSON (data as Any)
                    let id = dict[PARAMS.ID].intValue
                    let picture = dict[PARAMS.USER_PHOTO].stringValue
                    thisuser!.clearUserInfo()
                    thisuser!.id = id
                    thisuser!.user_photo = picture
                    thisuser!.name = self.name
                    thisuser!.user_name = self.user_name
                    thisuser!.email = self.email
                    thisuser!.phone_number = self.phone_number
                    thisuser!.password = self.password
                    thisuser!.location = self.location
                    thisuser!.industry = self.industry
                    thisuser!.website_link = self.website_link
                    thisuser!.user_type = user_type
                    thisuser!.remember_me = remember_me
                    thisuser!.notification = true
                    
                    thisuser!.saveUserInfo()
                    thisuser!.loadUserInfo()
                    dump(thisuser, name: "userinfo =========>")
                    //self.gotoNavPresent("RAMAnimatedTabBarController", fullscreen: true)
                    //self.gotoTabControllerWithIndex(0)
                }else{
                    if let data = data{
                        let statue = data  as! Int
                        if statue == 201{
                            self.showAlerMessage(message: Messages.EMAIL_EXIST)
                        }else if statue == 202{
                            self.showAlerMessage(message: Messages.IMAGE_UPLOAD_FAIL)
                        }else {
                            self.showAlerMessage(message: Messages.NETISSUE)
                        }
                    }else{
                        self.showAlerMessage(message: Messages.NETISSUE)
                    }
                }
            }
        }
    }
}

extension RegisterVC: GMSAutocompleteViewControllerDelegate {
  func viewController(_ viewController: GMSAutocompleteViewController, didAutocompleteWith place: GMSPlace) {
    // Get the place name from 'GMSAutocompleteViewController'
    // Then display the name in textField
    self.edt_location.text = place.name
    //self.userLat = "\(place.coordinate.latitude)"
    //self.userLon = "\(place.coordinate.longitude)"
    // Dismiss the GMSAutocompleteViewController when something is selected
    dismiss(animated: true, completion: nil)
  }
  func viewController(_ viewController: GMSAutocompleteViewController, didFailAutocompleteWithError error: Error) {
    // Handle the error
    print("Error: ", error.localizedDescription)
  }
  func wasCancelled(_ viewController: GMSAutocompleteViewController) {
    // Dismiss when the user canceled the action
    dismiss(animated: true, completion: nil)
  }
}

extension RegisterVC: ImagePickerDelegate1{
    
    func didSelect(image: UIImage?) {
        self.gotoUploadProfile(image)
    }
}
