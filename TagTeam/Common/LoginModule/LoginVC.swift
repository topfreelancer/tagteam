//
//  LoginVC.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit
import BEMCheckBox
import GDCheckbox
import SwiftyJSON

class LoginVC: BaseVC {

    @IBOutlet weak var chbRememberMe: BEMCheckBox!
    @IBOutlet weak var cus_businessCheck: GDCheckbox!
    @IBOutlet weak var cus_influenceCheck: GDCheckbox!
    @IBOutlet weak var edt_email: UITextField!
    @IBOutlet weak var edt_pwd: UITextField!
    
    var str_email = ""
    var str_pwd = ""
    var user_type = ""
    var businesschecked: Bool = true
    var rememberchecked: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        hideKeyboardWhenTappedAround()
        setUI()
    }
    
    func setUI() {
        self.edt_email.text = "josh@gmail.com"
        self.edt_pwd.text = "123456"
        chbRememberMe.boxType = .square
        self.hideNavBar()
    }
    
    func setSetCheckBoxs(_ index: Int) {
        if index == 1{
            if self.cus_businessCheck.isOn{
                self.cus_influenceCheck.isOn = false
            }else{
                self.cus_influenceCheck.isOn = true
            }
        }else{
            if self.cus_influenceCheck.isOn{
                self.cus_businessCheck.isOn = false
            }else{
                self.cus_businessCheck.isOn = true
            }
        }
    }
    @IBAction func checkBoxClicked(_ sender: Any) {
        self.rememberchecked = !self.rememberchecked
    }
    
    @IBAction func tappedSignin(_ sender: Any) {
        self.str_email = self.edt_email.text!
        self.str_pwd = self.edt_pwd.text!
        
        if str_email.isEmpty{
            self.showToast(Messages.EMAIL_REQUIRE)
            return
        }
        if !isValidEmail(testStr: str_email){
            self.showToast(Messages.VALID_EMAIL_REQUIRE)
            return
        }
        if str_pwd.isEmpty{
            self.showToast(Messages.PASSWORD_REQUIRE)
            return
        }else{
            self.createMenuVC()
            /*self.showLoadingView(vc: self)
            ApiManager.login(email: str_email, password: str_pwd) { (isSuccess, data) in
                self.hideLoadingView()
                if isSuccess{
                    thisuser?.clearUserInfo()
                    UserDefault.setString(key: PARAMS.PASSWORD, value: self.str_pwd)
                    UserDefault.setBool(key: PARAMS.REMEMBER_ME, value: self.chbRememberMe.isSelected)
                    let data = JSON(data as Any)
                    thisuser = UserModel(data)
                    thisuser?.password = self.str_pwd
                    thisuser?.remember_me = self.rememberchecked
                    thisuser?.saveUserInfo()
                    thisuser?.loadUserInfo()
                    self.createMenuVC()
                }else{
                    self.showToast(Messages.NETISSUE)
                }
            }*/
        }
    }
    
    /*func loadUserType(){
        if self.cus_businessCheck.isOn{
            self.user_type = Usertype.business.rawValue
        }else{
            self.user_type = Usertype.influence.rawValue
        }
    }*/
    
    @IBAction func businessCheckBtnClicked(_ sender: Any) {
        setSetCheckBoxs(1)
    }
    
    @IBAction func influenceCheckBtnClicked(_ sender: Any) {
        setSetCheckBoxs(2)
    }
}
