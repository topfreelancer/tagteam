//
//  RegisterAsVC.swift
//  TagTeam
//
//  Created by Admin on 9/30/20.
//

import UIKit

class RegisterAsVC: BaseVC {

    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    @IBAction func tappedBusiness(_ sender: Any) {
        UserDefault.setString(key: PARAMS.USER_TYPE, value: Usertype.business.rawValue)
        self.gotoNavPresent(VCs.REGISTER, fullscreen: true)
    }
    
    @IBAction func tappedInfluencer(_ sender: Any) {
        UserDefault.setString(key: PARAMS.USER_TYPE, value: Usertype.influence.rawValue)
        self.gotoNavPresent(VCs.REGISTER, fullscreen: true)
    }
    @IBAction func tappedBack(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
}
