//
//  UserModel.swift
//  EveraveUpdate
//
//  Created by Ubuntu on 1/18/20.
//  Copyright © 2020 Ubuntu. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserModel: NSObject {
    
    var id : Int!
    var name: String?
    var user_name : String?
    var user_photo : String?
    var email : String?
    var phone_number : String?
    var location : String?
    var industry: String?
    var website_link: String?
    var password: String?
    var user_type: String?
    var remember_me: Bool!
    var notification: Bool!
    
    override init() {
       super.init()
        id = 0
        name = ""
        user_name = ""
        user_photo = ""
        email = ""
        phone_number = ""
        location = ""
        industry = ""
        website_link = ""
        password = ""
        user_type = Usertype.business.rawValue
        remember_me = false
        notification = true
    }
    
    init(_ json : JSON) {
        self.id = json[PARAMS.ID].intValue
        self.name = json[PARAMS.NAME].stringValue
        self.user_name = json[PARAMS.USER_NAME].stringValue
        self.user_photo = json[PARAMS.USER_PHOTO].stringValue
        self.email = json[PARAMS.EMAIL].stringValue
        self.phone_number = json[PARAMS.PHONE_NUMBER].stringValue
        self.location = json[PARAMS.LOCATION].stringValue
        self.industry = json[PARAMS.INDUSTRY].stringValue
        self.website_link = json[PARAMS.WEBSITE_LINK].stringValue
        self.password = UserDefault.getString(key: PARAMS.PASSWORD,defaultValue: "")
        self.user_type = json[PARAMS.USER_TYPE].stringValue
        self.remember_me = UserDefault.getBool(key: PARAMS.REMEMBER_ME,defaultValue: false)
        self.notification = json[PARAMS.NOTIFICATION].stringValue == "1" ? true : false
    }
    // Check and returns if user is valid user or not
   var isValid: Bool {
       return id != nil && id != 0
   }
    
    // Recover user credential from UserDefault
    func loadUserInfo() {
       
        id = UserDefault.getInt(key: PARAMS.ID, defaultValue: 0)
        name = UserDefault.getString(key: PARAMS.NAME, defaultValue: "")
        user_name = UserDefault.getString(key: PARAMS.USER_NAME, defaultValue: "")
        user_photo = UserDefault.getString(key: PARAMS.USER_PHOTO, defaultValue: "")
        email = UserDefault.getString(key: PARAMS.EMAIL, defaultValue: "")
        phone_number = UserDefault.getString(key: PARAMS.PHONE_NUMBER, defaultValue: "")
        location = UserDefault.getString(key: PARAMS.LOCATION, defaultValue: "")
        industry = UserDefault.getString(key: PARAMS.INDUSTRY, defaultValue: "")
        website_link = UserDefault.getString(key: PARAMS.WEBSITE_LINK, defaultValue: "")
        password = UserDefault.getString(key: PARAMS.PASSWORD, defaultValue: "")
        user_type = UserDefault.getString(key: PARAMS.USER_TYPE, defaultValue: "business")
        remember_me = UserDefault.getBool(key: PARAMS.REMEMBER_ME, defaultValue: false)
        notification = UserDefault.getBool(key: PARAMS.NOTIFICATION, defaultValue: true)
    }
    // Save user credential to UserDefault
    func saveUserInfo() {
        UserDefault.setInt(key: PARAMS.ID, value: id)
        UserDefault.setString(key: PARAMS.NAME, value: name)
        UserDefault.setString(key: PARAMS.USER_NAME, value: user_name)
        UserDefault.setString(key: PARAMS.USER_PHOTO, value: user_photo)
        UserDefault.setString(key: PARAMS.EMAIL, value: email)
        UserDefault.setString(key: PARAMS.PHONE_NUMBER, value: phone_number)
        UserDefault.setString(key: PARAMS.LOCATION, value: location)
        UserDefault.setString(key: PARAMS.INDUSTRY, value: industry)
        UserDefault.setString(key: PARAMS.WEBSITE_LINK, value: website_link)
        UserDefault.setString(key: PARAMS.PASSWORD, value: password)
        UserDefault.setString(key: PARAMS.USER_TYPE, value: user_type)
        UserDefault.setBool(key: PARAMS.REMEMBER_ME, value: remember_me)
        UserDefault.setBool(key: PARAMS.NOTIFICATION, value: notification)
    }
    // Clear save user credential
    func clearUserInfo() {
        
        id = 0
        name = ""
        user_name = ""
        user_photo = ""
        email = ""
        phone_number = ""
        location = ""
        industry = ""
        website_link = ""
        user_type = ""
        remember_me = false
        notification = true
        
        UserDefault.setInt(key: PARAMS.ID, value: 0)
        UserDefault.setString(key: PARAMS.NAME, value: nil)
        UserDefault.setString(key: PARAMS.USER_NAME, value: nil)
        UserDefault.setString(key: PARAMS.USER_PHOTO, value: nil)
        UserDefault.setString(key: PARAMS.EMAIL, value: nil)
        UserDefault.setString(key: PARAMS.PHONE_NUMBER, value: nil)
        UserDefault.setString(key: PARAMS.LOCATION, value: nil)
        UserDefault.setString(key: PARAMS.INDUSTRY, value: nil)
        UserDefault.setString(key: PARAMS.WEBSITE_LINK, value: nil)
        UserDefault.setString(key: PARAMS.USER_TYPE, value: nil)
        UserDefault.setBool(key: PARAMS.REMEMBER_ME, value: false)
        UserDefault.setBool(key: PARAMS.NOTIFICATION, value: true)
    }
}

enum Usertype: String {
    case business = "business"
    case influence = "influence"
}

