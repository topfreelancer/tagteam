//
//  NotiModel&Cell.swift
//  emoglass
//
//  Created by Mac on 7/8/20.
//  Copyright © 2020 Mac. All rights reserved.
//


import Foundation
import Kingfisher

class FollowerModel{
    
    var id : String?
    var userName: String?
    var userPhoto: String?
    
    init(id: String,userName: String, userPhoto: String) {
        self.id = id
        self.userName = userName
        self.userPhoto = userPhoto
    }
}


class FollowerCell: UITableViewCell {
    @IBOutlet var imv_photo: UIImageView!
    @IBOutlet weak var userName: UILabel!
    
    var entity:FollowerModel!{
        didSet{
            let url = URL(string: entity.userPhoto ?? "")
            imv_photo.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
            userName.text = entity.userName
        }
    }
}

