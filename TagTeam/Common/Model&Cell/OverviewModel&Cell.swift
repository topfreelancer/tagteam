//
//  OverviewModel&Cell.swift
//  TagTeam
//
//  Created by top Dev on 10/4/20.
//

import Foundation
import Kingfisher

enum status {
    case ongoing
    case completed
    case canceled
    case under_review
}

class OverviewModel{
    
    var id : String?
    var userName: String?
    var userPhoto: String?
    var request_type : String?
    var distance: String?
    var industry: String?
    var date : String?
    var content_type: String?
    var content_sourcing: String?
    var status: status?
    var price: Float!
    
    init(id: String,userName: String, userPhoto: String,request_type: String,distance: String, industry: String,date: String,content_type: String, content_sourcing: String,status: status, price: Float) {
        self.id = id
        self.userName = userName
        self.userPhoto = userPhoto
        self.request_type = request_type
        self.distance = distance
        self.industry = industry
        self.date = date
        self.content_type = content_type
        self.content_sourcing = content_sourcing
        self.status = status
        self.price = price
    }
}


class OverviewCell: UITableViewCell {
    
    @IBOutlet var imv_photo: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var request_type: UILabel!
    @IBOutlet weak var distance: UILabel!
    @IBOutlet weak var industry: UILabel!
    @IBOutlet weak var date: UILabel!
    @IBOutlet weak var content_type: UILabel!
    @IBOutlet weak var content_sourcing: UILabel!
    @IBOutlet weak var ongoing: UILabel!
    @IBOutlet weak var completed: UILabel!
    @IBOutlet weak var canceled: UILabel!
    @IBOutlet weak var under_review: UILabel!
    
    var entity:OverviewModel!{
        didSet{
            let url = URL(string: entity.userPhoto ?? "")
            imv_photo.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
            userName.text = entity.userName
            request_type.text = "request type:" + " " + entity.request_type!
            distance.text = "Distance:" + " " + entity.distance!
            industry.text = "Industry:" + " " + entity.industry!
            date.text = "Date:" + " " + entity.date!
            content_type.text = "Content type:" + " " + entity.content_type!
            content_sourcing.text = "Content sourcing:" + " " + entity.content_sourcing!
            if let status = entity.status{
                setStatus(status)
            }
        }
    }
    
    func setStatus(_ status: status)  {
        switch status {
        case .ongoing:
            ongoing.textColor = .blue
            completed.textColor = .lightGray
            canceled.textColor = .lightGray
            under_review.textColor = .lightGray
        case .completed:
            ongoing.textColor = .lightGray
            completed.textColor = .blue
            canceled.textColor = .lightGray
            under_review.textColor = .lightGray
        case .canceled:
            ongoing.textColor = .lightGray
            completed.textColor = .lightGray
            canceled.textColor = .blue
            under_review.textColor = .lightGray
        case .under_review:
            ongoing.textColor = .lightGray
            completed.textColor = .lightGray
            canceled.textColor = .lightGray
            under_review.textColor = .blue
        }
    }
}

class PendingCell: UITableViewCell {
    
    @IBOutlet var imv_photo: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var request_title: UILabel!
    @IBOutlet weak var lbl_requestContent: UILabel!
    @IBOutlet weak var lbl_expireDate: UILabel!
    
    
    var entity:OverviewModel!{
        didSet{
            let url = URL(string: entity.userPhoto ?? "")
            imv_photo.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
            userName.text = entity.userName
            request_title.text = "request title:" + " " + entity.request_type!
            lbl_requestContent.text = "Request content:" + " " + entity.content_sourcing!
            lbl_expireDate.text = "Expired Date:" + " " + entity.date!
        }
    }
}

class CompletedCell: UITableViewCell {
    
    @IBOutlet var imv_photo: UIImageView!
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var request_title: UILabel!
    @IBOutlet weak var lbl_requestContent: UILabel!
    @IBOutlet weak var lbl_completedDate: UILabel!
    
    
    var entity:OverviewModel!{
        didSet{
            let url = URL(string: entity.userPhoto ?? "")
            imv_photo.kf.setImage(with: url,placeholder: UIImage(named: "avatar"))
            userName.text = entity.userName
            request_title.text = "request title:" + " " + entity.request_type!
            lbl_requestContent.text = "Request content:" + " " + entity.content_sourcing!
            lbl_completedDate.text = "Completed Date:" + " " + entity.date!
        }
    }
}
